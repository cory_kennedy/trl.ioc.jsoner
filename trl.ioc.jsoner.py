import os
import re
from pathlib import Path
from alive_progress import alive_bar
import time
from prettytable import PrettyTable
from colorama import Fore, Back, Style

base = os.getcwd()
domains = str(input(Fore.CYAN+"[Enter Filename]: "+Style.RESET_ALL+" Press enter for | domains.txt") or "domains.txt")
with alive_bar(23, title=Fore.GREEN+'Processing Domains!!!', bar='bubbles') as bar:
        for i in range(23):
            time.sleep(.000000000000000002)
            bar()  

for count, filename in enumerate(os.listdir(base)):
    for filename in os.listdir(base):
        with open(os.path.join(base, filename), "r") as dom_reader:
            for i, domain in enumerate(domains):
                try:
                    for i in dom_reader:    
                        if filename.endswith('.txt'):
                            for d in dom_reader:
                                if d:     
                                    jsonoutput ='''
    (
     "type": "domain",
     "value": "{}",
     "enterprise": false
    ),'''.format(d.rstrip())
                                    f = open('domains.json', "a")
                                    f.write(jsonoutput)
                                    f = open('domains.json', "r")
                                    filedata = f.read()
                                    fixit = filedata.replace('(','{').replace(')', '}')
                                    #newline = line.rstrip('\r\n')
                                    f = open('domains.json', "w")
                                    
                                    f.write(fixit)
                                    f.close()
                            print('\n '+Style.DIM+Fore.LIGHTBLUE_EX+'-----------------------------------------\n|'+Style.RESET_ALL+Fore.RED+Style.BRIGHT+'***Your input file has been renamed!!!***'+Style.DIM+Fore.LIGHTBLUE_EX+'|\n '+Style.DIM+Fore.LIGHTBLUE_EX+'-----------------------------------------\n'+Style.DIM+Fore.LIGHTBLUE_EX+'| '+Style.RESET_ALL+'From:  '+domains+  Style.DIM+Fore.LIGHTBLUE_EX+'|'+Style.RESET_ALL+' To: ' +Path(domains).stem+'.done'+Style.DIM+Fore.LIGHTBLUE_EX+'    |\n' +Style.DIM+Fore.LIGHTBLUE_EX+' -----------------------------------------\n'+Style.DIM+Fore.LIGHTBLUE_EX+'|      '+Style.RESET_ALL+'Output is here: '+Path(domains).stem+'.json       '      +Style.DIM+Fore.LIGHTBLUE_EX +'|\n' +Style.DIM+Fore.LIGHTBLUE_EX+' -----------------------------------------'+Style.RESET_ALL)
                            a_file = open(Path(domains).stem+'.json',"r")
                            number_of_lines = 13
                            for i in range(number_of_lines):
                                line = a_file.readline()
                                print("Sample Output:"+line.rstrip())
                    os.rename(os.path.join(base, Path(filename).stem+'.txt'), os.path.join(base, Path(filename).stem+'.done'))            
                        
                except OSError:
                    continue
